require "zip/file"
require "zip/streamable_stream"

require "tempfile"

#
# Monkeypatching issue with opening file not in binary mode
#

module Zip
  class File
    private

    def get_tempfile
      Tempfile.new(::File.basename(name), ::File.dirname(name), mode: ::File::BINARY).tap &:binmode
    end
  end

  class StreamableStream
    def initialize(entry)
      super(entry)
      dirname = if zipfile.is_a?(::String)
                  ::File.dirname(zipfile)
                else
                  '.'
                end
      @temp_file = Tempfile.new(::File.basename(name), dirname, mode: ::File::BINARY).tap &:binmode
    end
  end
end
