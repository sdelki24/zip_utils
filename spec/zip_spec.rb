#coding: utf-8
require_relative 'helper'
require 'stringio'
require 'fileutils'
require 'tempfile'
require 'digest/md5'
require 'tmpdir'

describe ZipUtils::Zip do

  def compare_directories_content(*directories)
    directories.map do |path|
      Dir["#{path}/**/*"].map { |v| [v.gsub(/^#{Regexp.escape(path)}\//, ''),
                                     File.directory?(v) ? 'dir' : Digest::MD5.hexdigest(File.binread(v))] }
    end.uniq.count == 1
  end


  # @example
  #
  #   subject(:files) { Something.method_to_test options }  # required!
  #                                                 # `options` will be defined by this shared examples
  #   include_examples 'files'
  #
  shared_examples 'files', :files do
    let(:options) { Hash.new }

    it 'returns Array' do
      expect(subject).to be_a_kind_of Array
    end

    it 'includes all file paths' do
      expected_files = %w(some/text.txt some/nesting/33906-030246-470216afb2402a0b176c49b2447b9305.jpg a_9316c282.jpg some/.hidden_file .hide_me_pls)
      # all filenames should be in binary encoding (without actual convertation to anything else)
      expected_files << 'some/текст.txt'.force_encoding(Encoding::BINARY)
      expect(subject).to match_array expected_files
    end

    context 'with :skip_hidden option set to true' do
      let(:options) do
        {skip_hidden: true}
      end

      it 'does not include .hidden files' do
        expect(subject).not_to include 'some/.hidden_file'
        expect(subject).not_to include '.hide_me_pls'
      end
    end
  end

  context 'reading' do
    context '#contain?' do
      shared_examples 'contain' do
        context 'when passed file exists in zip' do
          it 'returns true' do
            expect(subject.contain? 'some/text.txt').to be true
            expect(subject.contain? 'some/nesting/33906-030246-470216afb2402a0b176c49b2447b9305.jpg').to be true
            expect(subject.contain? 'a_9316c282.jpg').to be true
          end
        end

        context 'when passed file does not exist in zip' do
          it 'returns false' do
            expect(subject.contain?('some/no_one.txt')).to be false
            expect(subject.contain?('toto')).to be false
          end
        end
      end

      context 'with zip file' do
        subject { described_class.new "#{CASES_DIRECTORY}/correct/compressed.zip" }

        include_examples 'contain'
      end

      context 'with buffer' do
        subject { described_class.new File.binread("#{CASES_DIRECTORY}/correct/compressed.zip"), buffer: true }

        include_examples 'contain'
      end
    end

    context '#decompress' do

      # @example
      # subject{ ZipUtils::Zip.new(…) } # required!
      # include_examples 'decompress'
      shared_examples 'decompress' do
        it 'decompresses zip to directory' do
          Dir.mktmpdir do |tmp|
            tmp.force_encoding('utf-8')
            subject.decompress(tmp)
            expect(compare_directories_content(tmp, "#{CASES_DIRECTORY}/correct/extracted")).to be true
          end
        end
      end

      context 'with zip file' do
        subject { described_class.new "#{CASES_DIRECTORY}/correct/compressed.zip" }

        include_examples 'decompress'
      end

      context 'with buffer' do
        subject { described_class.new File.binread("#{CASES_DIRECTORY}/correct/compressed.zip"), buffer: true }

        include_examples 'decompress'
      end

      context 'with incorrect zip archive' do
        it 'raises ZipUtils::Errors::CanNotDecompress' do
          Dir.mktmpdir do |tmpdir|
            expect { described_class.new("#{CASES_DIRECTORY}/incorrect.zip").decompress(tmpdir) }.to raise_error(ZipUtils::Errors::CanNotDecompress)
          end
        end
      end
    end

    context '#content' do
      # @example
      # subject{ ZipUtils::Zip.new(…) } # required!
      # include_examples 'content'
      shared_examples 'content' do
        it 'fetches file content in zip' do
          expect(subject.content('some/text.txt')).to eq File.read("#{CASES_DIRECTORY}/correct/extracted/some/text.txt")

          # filename in binary encoding should not be converted at all
          expect(subject.content('some/текст.txt'.force_encoding(Encoding::BINARY))).to eq File.read("#{CASES_DIRECTORY}/correct/extracted/some/текст.txt")
        end
      end

      context 'with zip file' do
        subject { described_class.new("#{CASES_DIRECTORY}/correct/compressed.zip") }

        include_examples 'content'
      end

      context 'with buffer' do
        subject { described_class.new File.binread("#{CASES_DIRECTORY}/correct/compressed.zip"), buffer: true }

        include_examples 'content'
      end

      context 'with incorrect zip archive' do
        it 'raises ZipUtils::Errors::CanNotDecompress' do
          expect { described_class.new("#{CASES_DIRECTORY}/incorrect.zip").content('some.txt') }.to raise_error ZipUtils::Errors::CanNotDecompress
          expect { described_class.new("#{CASES_DIRECTORY}/first.pdf.zip").content('some.txt') }.to raise_error ZipUtils::Errors::CanNotDecompress
        end
      end

      context 'with unknown file path passed' do
        it 'raises ZipUtils::Errors::CanNotDecompress' do
          expect { described_class.new("#{CASES_DIRECTORY}/first.pdf.zip").content('some.txt') }.to raise_error ZipUtils::Errors::CanNotDecompress
        end
      end
    end

    context '#files' do
      # `options` defined in 'files' shared examples

      context 'with zip file', :files do
        subject(:files) { described_class.new("#{CASES_DIRECTORY}/correct/compressed.zip").files options }
      end

      context 'with buffer', :files do
        subject(:files) { described_class.new(File.binread("#{CASES_DIRECTORY}/correct/compressed.zip"), buffer: true).files options }
      end

      context 'with incorrect zip archive' do
        it 'raises ZipUtils::Errors::CanNotDecompress' do
          expect { described_class.new("#{CASES_DIRECTORY}/incorrect.zip").files }.to raise_error(ZipUtils::Errors::CanNotDecompress)
        end
      end
    end
  end

  context 'Zip() sugar' do
    it 'creates new Zip object' do
      expect(ZipUtils::Zip("#{CASES_DIRECTORY}/correct/compressed.zip")).to be_a_kind_of(described_class)
    end
  end

  context 'modifying' do
    context '#remove' do
      let!(:zip_tempfile) { Tempfile.new('archive.zip').tap(&:close) }
      let!(:zip_file_path) do
        zip_tempfile.path.tap { |path| FileUtils.cp "#{CASES_DIRECTORY}/correct/compressed.zip", path }
      end

      let(:zip) { described_class.new zip_file_path }

      context 'with binary string archive' do
        it 'raises ZipUtils::Errors::ReadOnly' do
          zip = described_class.new(File.binread(zip_file_path), buffer: true)
          expect { zip.remove 'some/text.txt' }.to raise_error(ZipUtils::Errors::ReadOnly)
        end
      end

      context 'with file' do
        before { zip.remove 'some/text.txt', 'a_9316c282.jpg' }
        it 'removes passed files from zip' do
          expect(zip.contain?('some/text.txt')).to be false
          expect(zip.contain?('a_9316c282.jpg')).to be false
        end

        it 'keeps another files in their places' do
          expect(zip.contain?('some/nesting/33906-030246-470216afb2402a0b176c49b2447b9305.jpg')).to be true
        end
      end
    end

    context '#add_hierarchy' do
      context 'with binary string archive' do
        it 'raises ZipUtils::Errors::ReadOnly' do
          zip = described_class.new('', buffer: true)
          expect { zip.add_hierarchy "#{CASES_DIRECTORY}/correct/extracted/", 'bla/' }.to raise_error(ZipUtils::Errors::ReadOnly)
        end
      end

      context 'with file' do
        # @example
        # before{ described_class.new(…).add_hierarchy(…) } # required!
        # let(:tmp){ '/some/tmp/directory/' } # required!
        # include_examples 'adds directory', true # pass true if added files should not be compressed
        shared_examples 'adds directory' do |without_compression|
          it 'adds passed directory to zip' do
            Dir.mkdir("#{tmp}/extracted/")
            described_class.new("#{tmp}/test.zip").decompress("#{tmp}/extracted/")

            expect(compare_directories_content("#{tmp}/extracted/bla/", "#{CASES_DIRECTORY}/correct/extracted/")).to be true
          end
        end

        let!(:tmp) { Dir.mktmpdir.force_encoding 'utf-8' }
        after { FileUtils.rm_rf tmp }

        let(:extracted_size) { Dir["#{CASES_DIRECTORY}/correct/extracted/**/*"].inject(0) { |p, f| File.file?(f) ? p+File.size(f) : p } }

        context 'with compression (default)' do
          before { described_class.new("#{tmp}/test.zip").add_hierarchy("#{CASES_DIRECTORY}/correct/extracted/", 'bla/') }

          include_examples 'adds directory', false

          it 'compresses all files' do
            expect(described_class.new("#{tmp}/test.zip")).to be_compressed
            expect(File.size("#{tmp}/test.zip")).to be < extracted_size
          end
        end

        context 'without compression' do
          before { described_class.new("#{tmp}/test.zip").add_hierarchy("#{CASES_DIRECTORY}/correct/extracted/", 'bla/', true) }

          include_examples 'adds directory', true

          it 'does not compress any files' do
            expect(described_class.new("#{tmp}/test.zip")).not_to be_compressed
            expect(File.size("#{tmp}/test.zip")).to be >= extracted_size
          end
        end

        context 'when not existing directory passed' do
          it 'raises ZipUtils::Errors::DirectoryOrFileDoesNotExists' do
            Dir.mktmpdir do |tmpdir|
              expect { described_class.new("#{tmpdir}/data.zip").add_hierarchy("#{CASES_DIRECTORY}/directory_that_not_exists/") }.to raise_error(ZipUtils::Errors::DirectoryOrFileDoesNotExists)
            end
          end
        end
      end

    end

    context '#add' do
      context 'with binary string archive' do
        it 'raises ZipUtils::Errors::ReadOnly' do
          zip = described_class.new('', buffer: true)
          expect { zip.add('a', 'b') }.to raise_error(ZipUtils::Errors::ReadOnly)
        end
      end

      context 'with file' do
        let(:zip_container) { described_class.new("#{tmp}/test.zip") }
        let(:test_content) { 'yahoo' * 100 }

        def compress(without_compression)
          zip_container.add('hehey', test_content, without_compression)

          zip_container.add('hehey2', 'bla bla', without_compression)
          zip_container.add('hehey2', test_content, without_compression)
        end

        shared_examples 'adds content' do
          it 'adds passed content as a file in zip' do
            expect(zip_container.content('hehey')).to eq test_content
          end

          it 'replaces already existed file in zip' do
            expect(zip_container.content('hehey2')).to eq test_content
          end
        end


        let!(:tmp) { Dir.mktmpdir }
        after { FileUtils.rm_rf tmp }

        let(:zip_file_path) { "#{tmp}/test.zip" }
        before { FileUtils.cp "#{CASES_DIRECTORY}/correct/compressed.zip", zip_file_path }
        let!(:source_zip_size) { File.size zip_file_path }

        context 'with compression (default)' do
          before { compress false }

          include_examples 'adds content'

          it 'compresses added file' do
            expect(File.size zip_file_path).to be < (source_zip_size+test_content.bytesize)
          end
        end


        context 'without compression' do
          before { compress true }

          include_examples 'adds content'

          it 'does not compress added file' do
            expect(File.size zip_file_path).to be >= (source_zip_size+test_content.bytesize)
          end
        end


      end
    end
  end


  describe '.zip' do

    # @example
    # let(:file_path){ 'some file path' } # required!
    # let(:buffer){ true }                # optional, false by default
    # it_behaves_like 'zip magic', true   # last option specifies whether we need to check
    #                                     # changes in actual file (file_path) or not
    shared_examples 'zip magic' do |check_file|

      # @example
      # let(:zip_file){ '/some/path/to.zip' } # required
      # it_behaves_like 'zip file'
      shared_examples 'zip file' do
        it 'has added file' do
          # yep, we'll check using our own code
          expect(ZipUtils::Zip(zip_file).content('test.txt')).to eq 'Test Text'
        end
      end

      subject! do
        described_class.zip(file_path, buffer: begin
                                       buffer rescue false
                                     end) do |zip|
          zip.add('test.txt', 'Test Text')
        end
      end

      it 'returns binary string with zip archive' do
        expect(subject).to be_a_kind_of(String)
        expect(subject).not_to be_empty
      end

      context 'returned binary string' do
        let!(:zip_file) { Tempfile.new('test.zip').tap(&:close).path }
        before { File.open(zip_file, 'wb+') { |f| f << subject } }
        after { File.unlink zip_file }

        include_examples 'zip file'
      end

      context 'modified zip file' do
        let(:zip_file) { file_path }
        include_examples 'zip file'
      end if check_file
    end

    context 'with file path' do
      let!(:file_path) { Tempfile.new('test.zip').tap(&:close).path }
      after { File.unlink file_path }

      context 'with new archive' do
        include_examples 'zip magic'
      end

      context 'with existing archive' do
        before do
          ZipUtils::Zip(file_path).add('existing.txt', 'I exist')
        end

        include_examples 'zip magic'

        it 'does not affect existing data in archive' do
          expect(ZipUtils::Zip(file_path).content('existing.txt')).to eq 'I exist'
        end
      end

    end

    context 'without file path' do
      let(:file_path) { nil }
      include_examples 'zip magic'
    end

    context 'with :buffer option' do
      let(:file_path) { '' }
      let(:buffer) { true }

      include_examples 'zip magic'
    end
  end

  describe '.content' do
    let(:zip_file_path) { "#{CASES_DIRECTORY}/correct/compressed.zip" }
    let(:zip_file_content) { File.binread zip_file_path }

    shared_examples 'content extracting' do
      it 'returns decompressed file content' do
        expect(subject).to eq(File.read("#{CASES_DIRECTORY}/correct/extracted/some/text.txt"))
      end
    end

    context 'by default' do
      subject { described_class.content zip_file_path, 'some/text.txt' }

      include_examples 'content extracting'
    end

    context 'with :buffer option' do
      subject { described_class.content zip_file_content, 'some/text.txt', buffer: true }

      include_examples 'content extracting'
    end
  end

  describe '.files' do
    # `options` defined in 'files' shared examples

    context 'by default', :files do
      subject(:files) { described_class.files "#{CASES_DIRECTORY}/correct/compressed.zip", options }
    end

    context 'with :buffer option', :files do
      subject(:files) { described_class.files File.binread("#{CASES_DIRECTORY}/correct/compressed.zip"), options.merge(buffer: true) }
    end

    context 'with incorrect zip archive' do
      it 'raises ZipUtils::Errors::CanNotDecompress' do
        expect { described_class.files "#{CASES_DIRECTORY}/incorrect.zip" }.to raise_error ZipUtils::Errors::CanNotDecompress
      end
    end
  end


  describe 'handling data with "\x1A" char at the end' do
    let(:data){ "0\x1A" }

    context 'using buffer' do
      it 'succeeds' do
        zip_content = ZipUtils::Zip.zip('', buffer: true) do |zip|
          zip.add '1.bin', data
        end
        zip = ZipUtils::Zip zip_content, buffer: true
        expect(zip.content('1.bin')).to eq data
      end
    end

    context 'without buffer' do
      let(:arch_path){ Tempfile.new('test.zip').tap(&:close).path }
      
      it 'succeeds' do
        ZipUtils::Zip.zip(arch_path, buffer: false) do |zip|
          zip.add '1.bin', data
        end
        zip = ZipUtils::Zip arch_path, buffer: false
        expect(zip.content('1.bin')).to eq data
      end

      after { FileUtils.remove_entry_secure(arch_path) if File.file?(arch_path) }
    end

  end

  describe 'handling non ASCII file names' do
    let(:arch_path){ Tempfile.new('test.zip').tap(&:close).path }

    def get_zip(&block)
      FileUtils.remove_entry_secure(arch_path) if File.file?(arch_path)
      ZipUtils::Zip.zip(arch_path, buffer: false, &block)
      ZipUtils::Zip arch_path, buffer: false
    end

    shared_examples 'handling non ASCII file names somehow' do
      describe 'cyrillic' do
        describe 'using #add' do
          it 'handles filenames' do
            zip = get_zip{|z| z.add 'Отличный текстЪ Ё.bin', '012' }
            expect(zip).to be_contain('Отличный текстЪ Ё.bin')
            expect(zip.content('Отличный текстЪ Ё.bin')).to eq '012'
          end
        end

        describe 'using #add_hierarchy' do
          it 'handles filenames' do
            Dir.mktmpdir do |tmpdir|
              # В Windows tmpdir возвращается в кодировке IBM***,
              # поэтому конвертируем в UTF-8, чтобы далее нормально подставлялись символы Юникода
              tmpdir.force_encoding('utf-8')
              inner_dir = "#{tmpdir}/Fänrik"
              FileUtils.mkdir(inner_dir)
              File.open("#{inner_dir}/Øst.txt", 'wb'){|f| f<<'000'};
              zip = get_zip{|z| z.add_hierarchy tmpdir, 'Ståls/sägner' }
              expect(zip).to be_contain('Ståls/sägner/Fänrik/Øst.txt')
              expect(zip.content('Ståls/sägner/Fänrik/Øst.txt')).to eq '000'
            end
          end
        end

        describe 'when removing' do
          it 'handles filenames' do
            zip = get_zip{|z| z.add 'Отличный текстЪ Ё.bin', '012' }
            zip.remove('Отличный текстЪ Ё.bin')
            expect(zip).to_not be_contain('Отличный текстЪ Ё.bin')
          end
        end
      end
    end

    context 'when Zip.default_filename_encoding has not been set' do
      it_behaves_like 'handling non ASCII file names somehow'
    end

    describe 'when Zip.default_filename_encoding has been set' do

      before {
        @old_encoding = ZipUtils::Zip.default_filename_encoding
        ZipUtils::Zip.default_filename_encoding = 'cp866'
      }

      it_behaves_like 'handling non ASCII file names somehow'

      it 'uses exactly that encoding' do
        zip = get_zip{|z| z.add 'Отличный текстЪ Ё.bin', '012' }
        filename_cp866 = 'Отличный текстЪ Ё.bin'.encode('cp866').force_encoding(Encoding::BINARY)
        ::Zip::File.open(arch_path) do |low_level_zip|
          expect(low_level_zip.entries).to be_any{|e| e.name == filename_cp866}
          expect(low_level_zip.read(filename_cp866)).to eq '012'
        end

        Dir.mktmpdir do |tmpdir|
          inner_dir = "#{tmpdir}/документы"
          FileUtils.mkdir(inner_dir)
          File.open("#{inner_dir}/счёт на оплату.pdf", 'wb'){|f| f<<'000'};
          zip = get_zip{|z| z.add_hierarchy tmpdir, 'папка/ололо' }
          filename_cp866 = 'папка/ололо/документы/счёт на оплату.pdf'.encode('cp866').force_encoding(Encoding::BINARY)
          ::Zip::File.open(arch_path) do |low_level_zip|
            expect(low_level_zip.entries).to be_any{|e| e.name == filename_cp866}
            expect(low_level_zip.read(filename_cp866)).to eq '000'
          end
        end
      end

      after { ZipUtils::Zip.default_filename_encoding = @old_encoding }

    end

    after { FileUtils.remove_entry_secure(arch_path) if File.file?(arch_path) }
  end

end