#coding: utf-8
module ZipUtils
  module Errors
    class Base < ::StandardError
    end
    class CanNotDecompress < Base
    end
    class Internal < Base
    end
    class DirectoryOrFileDoesNotExists < Base
    end
    class ReadOnly < Base
    end
  end
end