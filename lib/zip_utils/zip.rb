#coding: utf-8
require 'tempfile'

module ZipUtils
  #thx for idea 2 Nokogiri::XML()
  class << self
    #Create instance of Zip class
    #[just syntax sugar, nothing special]
    #@see Zip#initialize
    def Zip(*args)
      Zip.new(*args)
    end
  end

  module Tmp
    def Tmp.new_file(basename='archive.zip')
      ::Tempfile.new(basename, nil, mode: File::BINARY).tap &:binmode
    end
  end

  # @note There are a lot of problems with `RubyZip` buffers
  #       including errors during data reading: {https://github.com/rubyzip/rubyzip/issues/199 issue #199} and {https://github.com/rubyzip/rubyzip/issues/30 issue #30}
  #       So, writing to buffers does not work in `ZipUtils::Zip` (such archives will be *readonly*)
  #       and one should use `ZipUtils::Zip#zip` to emulate string buffers working.
  class Zip
    # @param  [String]  zip     Path to zip file
    # @param  [Hash]    options
    # @option [Boolean] :buffer (false) pass `true` if `zip` is a string buffer (binary zip content),
    #                                   note that you *can not modify* such archive (it will be readonly)
    def initialize(zip, options = {})
      @buffer = !!options[:buffer]

      unless @buffer
        @zip_file = zip
      else
        @zip_file = Tmp.new_file
        @zip_file << zip
        @zip_file.close
      end
    end

    # Extract (unzip) archive to `destination`
    # If some file in archive is symlink, then content of written file will contain symlink path, not symlink destination file content!
    #
    # @param   destination   [String]  path to destination folder for extraction
    def decompress(destination)
      _open do |ar|
        ar.each do |zf|
          # If entry name encoded in binary then
          #    try to force it to the default filename encoding.
          # If it gets invalid then fall back to the binary encoding.
          # It's done for correct conversion of non-ANSI filenames on Windows.
          if zf.name.encoding == Encoding::BINARY
            zf.name.force_encoding(Zip.default_filename_encoding)
            unless zf.name.valid_encoding?
              zf.name.force_encoding(Encoding::BINARY)
            end
          end

          entry_path = "#{destination}/#{zf.name}"

          if zf.directory?
            FileUtils.mkdir_p(entry_path)
          else
            dirname = File.dirname(entry_path)
            FileUtils.mkdir_p(dirname) unless File.exist?(dirname)

            File.open(entry_path, 'wb') do |f|
              f << zf.get_input_stream.read
            end
          end
        end
      end
    rescue Exception => err
      raise Errors::CanNotDecompress.new(err)
    end

    alias_method :unzip, :decompress
    alias_method :extract, :decompress


    # Fetch content of file inside archive
    #
    # @param   [String]  path  path to file in zip archive
    # @return  [String]  content (symlink destination path if `path` is symlink)
    def content(path)
      _open do |zf|
        zf.read(encoded(path))
      end
    rescue Exception => err
      raise Errors::CanNotDecompress.new(err)
    end

    # Returns file paths inside archive without paths to directories.
    #
    # @example
    #
    #     # Given: zip with compressed files: "a/b.txt", "a/.hide_me" "c.txt", "d/" (directory)
    #     zip.files # => ['a/b.txt', 'a/.hide_me', 'c.txt']
    #     zip.files(skip_hidden: true) # => ['a/b.txt', 'c.txt']
    #
    # @since 0.3.3
    # @param  [Hash] options
    # @option options [Boolean] :skip_hidden (false) skip ".*" files in result
    # @raise  [Errors::CanNotDecompress] if zip is corrupted
    # @return [Array<String>] file paths
    def files(options = {})
      result = _open { |zf| zf.glob '**/*' }.map(&:name).reject { |file| file[-1] == '/' }
      options[:skip_hidden] ? result.reject { |file| file =~ /(\A|\/)\.[^\/]+\Z/ } : result
    rescue Exception => err
      raise Errors::CanNotDecompress.new(err)
    end

    # Add some directory hierarchy to zip archive
    #
    # @example Zip /home/vasya to vasya/files inside zip without compression
    #
    #    zip.add_hierarchy '/home/vasya/', 'vasya/files', true
    #
    # @param  [String]  source_path         path to directory in file system
    # @param  [String]  destination_path    path in zip archive
    # @param  [Boolean] without_compression
    def add_hierarchy(source_path, destination_path='', without_compression=false)
      raise(Errors::ReadOnly) if readonly?
      raise(Errors::DirectoryOrFileDoesNotExists, source_path) unless Dir.exist?(source_path)

      destination_path+='/' unless destination_path=='' || destination_path[-1]=='/'
      flags = [::Zip::File::CREATE]
      # flags << ::Zip::NO_COMPRESSION if without_compression
      _open(*flags) do |zf|
        Dir["#{source_path}/**/*"].each do |path|
          relative_path = path.encode('utf-8').gsub /^#{Regexp.escape(source_path)}\//, ''
          entry_path = encoded("#{destination_path}#{relative_path}")
          if File.directory?(path)
            zf.dir.mkdir(entry_path)
          else
            if without_compression
              entry = ::Zip::Entry.new '', entry_path
              entry.compression_method = ::Zip::Entry::STORED
              zf.add entry, path
            else
              zf.add(entry_path, path)
            end
          end
        end
      end
    end

    # Add (replace) one file by string content to zip archive
    #
    # @example  add file 'vasya/my_name.txt' with content 'Vasya' without compression
    #
    #    zip.add 'vasya/my_name.txt', 'Vasya', true
    #
    # @param  [String]  file_path path inside zip archive (file name)
    # @param  [String]  content   content of file
    # @param  [Boolean] without_compression
    def add(file_path, content, without_compression=false)
      raise(Errors::ReadOnly) if readonly?

      file_path = encoded(file_path)

      tempfile = nil
      begin
        flags = [::Zip::File::CREATE]

        _open(*flags) do |zf|
          zf.remove(file_path) if zf.find_entry(file_path)

          if without_compression
            tempfile = Tmp.new_file
            begin
              tempfile << content
              tempfile.close
              entry = ::Zip::Entry.new '', file_path
              entry.compression_method = ::Zip::Entry::STORED
              zf.add entry, tempfile.path
            end
          else
            zf.file.open(file_path, 'wb') { |f| f << content }
          end
        end
      ensure
        if tempfile
          tempfile.close unless tempfile.closed?
          tempfile.unlink
        end
      end
    end

    # Removes files in zip archive
    #
    # @example remove two files in archive
    #
    #    zip.remove 'a.txt', 'a/b.txt'
    #
    # @since 0.2.2
    # @param  [String]  file_paths paths inside zip archive (file name)
    def remove(*file_paths)
      raise(Errors::ReadOnly) if readonly?

      _open do |zf|
        zf.file.unlink *(file_paths.map{|f| encoded(f)})
      end
    end

    alias_method :rm, :remove

    # Check if zip archive contains file on `file_path`
    #
    # @since 0.2.2
    # @param  [String]  file_path path inside zip archive (file name)
    # @raise  [Errors::CanNotDecompress]  if zip is corrupted or there is no file on `file_path`
    # @return [Boolean]
    def contain?(file_path)
      _open { |zf| !!zf.find_entry(encoded(file_path)) }
    rescue Exception => err
      raise Errors::CanNotDecompress.new(err)
    end

    alias_method :include?, :contain?

    # @return [Boolean]
    def compressed?
      catch :result do
        _open do |zf|
          zf.each do |f|
            next if f.directory?
            throw(:result, true) if f.compressed_size < f.size
          end
        end
        throw(:result, false)
      end
    end

    class << self
      # Extract (unzip) `zip_file` to `destination`
      #
      # @param zip_file    [String]  path to zip file
      # @param destination [String]  path to destination folder for extraction
      def decompress (zip_file, destination)
        Zip.new(zip_file).decompress(destination)
      end

      alias_method :extract, :decompress
      alias_method :unzip, :decompress

      # Creates (changes) zip archive on the fly
      #
      # @example  create zip archive with one file
      #
      #     ZipUtils::Zip.zip{|z| z.add('some.txt', 'SOME') } # => zip binary string
      #
      # @example  change content of existing file in archive
      #
      #     ZipUtils::Zip.zip('zipper.zip') do |z|
      #       old_content = z.content('some.txt')
      #       z.add('some.txt', "Old content: #{old_content}")
      #     end # => zip binary string (zipper.zip also changed)
      #
      # @example  work with buffer
      #
      #     zip_binary = ZipUtils::Zip.zip(File.binread('zipper.zip'), buffer: true) do |z|
      #       z.add('some.txt', 'Hello there!')
      #     end
      #     ZipUtils::Zip.content(zip_binary, 'some.txt', buffer: true) # => 'Hello there!'
      #
      #     # by the way, this will not work as you would think (buffers are readonly!)
      #     zip_binary = File.binread('zipper.zip')
      #     ZipUtils::Zip.zip(zip_binary, buffer: true){|z| z.add('file', 'content') }
      #     ZipUtils::Zip.content(zip_binary, 'file', buffer: true) # => not 'content'!
      #
      # @example change buffer
      #
      #     zip_binary.replace(ZipUtils::Zip.zip(zip_binary, buffer: true){|z| … })
      #
      # @note buffers can be actually changed (using String#replace), but we need to maintain same behaviour in whole class,
      #       so buffers by themselves are *readonly* just because it will not be non-expected behaviour
      #
      # @param  [String, NilClass]    zip
      # @param  [Hash]                options
      #
      # @option options [Boolean]     :buffer (false) pass `true` when `zip` is not an file path, but binary zip string
      #                               note that buffers are *readonly*, thus you need to use returned
      #                               binary string (see examples)
      # @yield  [ZipUtils::Zip]
      # @return [String]  binary content of zip archive
      def zip(zip = nil, options = {}, &block)
        tempfile = nil

        if options[:buffer] || zip.nil?
          tempfile = Tmp.new_file
          tempfile << zip if options[:buffer]
          tempfile.close
          zip_file_path = tempfile.path
        else
          zip_file_path = zip
        end

        block.yield ZipUtils::Zip(zip_file_path)

        File.binread(zip_file_path)
      end

      # Extract some content from zip archive (file or string content aka *buffer*)
      #
      #
      # @example extract content from zip file
      #
      #    ZipUtils::Zip.content('file.zip', 'some/path.txt') # => 'path content'
      #
      # @example extract content from buffer
      #
      #    ZipUtils::Zip.content(File.binread('file.zip'), 'some/path.txt') # => 'path content'
      #
      # @param  [String]  zip_file  file path or binary string buffer (when `:buffer` option is specified)
      # @param  [String]  path      file path inside zip archive
      # @param  [Hash]    options
      # @option options [Boolean] :buffer (false)  pass true if `zip_file` is a string content of zip file (not a file path)
      # @raise  (see #content)
      # @return [String]
      def content(zip_file, path, options = {})
        new(zip_file, options).content(path)
      end

      # {include:Zip#files}
      #
      # @param  zip_file (see .content)
      # @param  [Hash]  options
      # @option (see .content)
      # @option (see #files)
      # @raise  (see #files)
      # @return (see #files)
      def files(zip_file, options = {})
        zip_options, options = {}, options.dup
        zip_options[:buffer] = options.delete :buffer

        new(zip_file, zip_options).files(options)
      end

      def default_filename_encoding
        @default_filename_encoding ||= Encoding::UTF_8
      end

      def default_filename_encoding=(enc)
        @default_filename_encoding = Encoding.find(enc)
      end

      def encoded(file_path)
        if file_path.encoding == Encoding::BINARY
          file_path
        else
          file_path.encode(
            default_filename_encoding, invalid: :replace, undef: :replace, replace: '_'
          ).force_encoding(Encoding::BINARY)
        end
      end
    end

    protected
    # @param  (see Zip::File.open)
    def _open(*params, &block)
      path = case @zip_file
      when File, Tempfile then @zip_file.path
      else
        @zip_file
      end
        
      ::Zip::File.open(*([path]+params), &block)
    end

    def readonly?
      @buffer
    end

    def encoded(file_path)
      self.class.encoded(file_path)
    end
  end
end
