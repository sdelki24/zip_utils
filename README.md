ZipUtils
========

`ZipUtils` provides `ZipUtils::Zip` class that works as a high-level abstraction over `RubyZip` library.

For usage examples and useful information please read *YARD* documentation:

    $ gem install yard redcarpet
    $ yard server --reload