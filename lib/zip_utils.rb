#coding: utf-8
require 'zip_utils/version'

require 'zip'
require 'zip/filesystem'

require 'fileutils'
require 'tempfile'
require 'tmpdir'

require 'monkeypatch/rubyzip' if /(win|mingw)/ === RUBY_PLATFORM

module ZipUtils
end

require_relative 'zip_utils/errors'
require_relative 'zip_utils/zip'
